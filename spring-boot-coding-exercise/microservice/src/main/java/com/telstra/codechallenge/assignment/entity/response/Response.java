package com.telstra.codechallenge.assignment.entity.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.telstra.codechallenge.assignment.entity.Items;

public class Response {

	@JsonProperty 
	private List<Items> items;

	public List<Items> getItems() {
		return items;
	}

	public void setItems(List<Items> items) {
		this.items = items;
	}
	


}
