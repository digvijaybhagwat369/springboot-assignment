package com.telstra.codechallenge.assignment.common;

import java.util.List;

import com.telstra.codechallenge.assignment.entity.Items;
import com.telstra.codechallenge.assignment.entity.response.Response;

public interface UtilityFun {
	
	public List<Items> getFormatedList(Response response,int record);

}
