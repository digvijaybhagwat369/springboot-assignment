package com.telstra.codechallenge.assignment.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.test.context.junit4.SpringRunner;

import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


import com.telstra.codechallenge.assignment.entity.Items;
import com.telstra.codechallenge.assignment.entity.Owner;
import com.telstra.codechallenge.assignment.entity.response.Response;

@RunWith(SpringRunner.class)
public class GitServiceTest {
	  
	
	@Mock
    private RestTemplate restTemplate;
	
	@MockBean
	 private GitService gitService;
     
     @Before
     public void setUp() {
       MockitoAnnotations.initMocks(this);
     }

	
	@Test
	 public void testGetGitUser() throws RestClientException,JSONException {
	
	Response response=new Response();
	Items item=new Items();
	Owner owner=new Owner();
	owner.setLogin("daniel-e");	
	item.setId("68911683");
	item.setHtml_url("https://github.com/daniel-e/tetros");
	item.setOwner(owner);
	List<Items> l=new ArrayList<Items>();
	l.add(item);
	response.setItems(l);
	
	Mockito
    .when(restTemplate.getForEntity(
      "https://api.github.com/search/repositories?q=", Response.class))
    .thenReturn(new ResponseEntity(response, HttpStatus.OK));
	
	Mockito.when(gitService.getGitUser("q", "stars", "desc", 1)).
	thenReturn(response.getItems());
	
	}
	

}
